git log show changes in commits
git diff compare repository with last commit
git mv rename files in filesystem and repository
git rm remove files in filesystem and repository
--cached remove files from repository but not filesystem
git clone clone a remote repository
git checkout checkout a new branch
git checkout -b create and checkout a new branch
git push push your committed changes to the remote repository
-u origin set the remote repository branch you want to commit to
git pull download changes from remote repository