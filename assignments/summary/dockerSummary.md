
docker attach	Attach local standard input, output, and error streams to a running container
docker build	Build an image from a Dockerfile
docker builder	Manage builds
docker checkpoint	Manage checkpoints
docker commit	Create a new image from a container’s changes
docker config	Manage Docker configs
docker container	Manage containers
docker context	Manage contex